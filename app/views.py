from django.shortcuts import render, get_object_or_404

from .models import Dog


def home(request):
    return render(request, 'app/home.html')

def dog_list(request):
    dogs = Dog.objects.all()
    return render(request, 'app/dogs.html', {'dogs': dogs})


def dog_profile(request, pk):
    dog = get_object_or_404(Dog, pk=pk)
    return render(request, 'app/dog_profile.html', {'dog': dog})