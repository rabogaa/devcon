from django.conf.urls import include, url

import app.views


urlpatterns = [
    url(r'^$', app.views.home, name='home'),
    url(r'^dogs/$', app.views.dog_list, name='dogs'),
    url(r'^dogs/(?P<pk>\d+)/$', app.views.dog_profile, name='dog_profile'),
    ]